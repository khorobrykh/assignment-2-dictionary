%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define stdin 0
%define stdout 1
%define stderr 2
%define exit_code 60
%define err_ret_code 1
%define success_ret_code 0
%define buf_size 256

section .bss
buffer: resb buf_size

section .rodata
err_msg_length:	db 'Error: The key is too long', 0
err_msg_exists:	db 'Error: The key does not exist', 0

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, buf_size
	call read_word
	test rax, rax
	je .fail_length

	mov rdi, rax
	mov rsi, element
	push rdx
	call find_word
	pop rdx
	test rax, rax
	je .fail_exists
	add rax, 8
	mov rdi, rax
	mov rax, rdx
	inc rdi
	add rdi, rdx
	mov rsi, stdout
	call print_string
	xor rax, rax
	jmp .ex
	
	.fail_length:
		mov rdi, err_msg_length
		mov rsi, stderr
		call print_string
		mov rax, err_ret_code
		jmp .ex

	.fail_exists:
		mov rdi, err_msg_exists
		mov rsi, stderr
		call print_string
		mov rax, err_ret_code

	.ex:
		push rax
		mov rsi, stdin
		call print_newline
		pop rax
		jmp exit
