COMPILE = nasm
FLAGS = -f elf64 -g -F dwarf

%.o: %.asm
	$(COMPILE) $(FLAGS) -o $@ $^

dict.o:	dict.asm
	$(COMPILE) $(FLAGS) -o $@ $<

main.o:	main.asm lib.o dict.o colon.inc
	$(COMPILE) $(FLAGS) -o $@ $<

build: main.o dict.o lib.o
	ld -o app main.o dict.o lib.o

clean:
	rm -f main.o dict.o app

rebuild:
	clean
	build

test: build
	python3.10 test.py
