%include "lib.inc"

section .text
global find_word

; rdi - указатель на нуль-терминированную строку
; rsi - указатель на начало словаря
find_word:
	.loop:
		test rsi, rsi
		jz .fail
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		test rax, rax
		jne .find
		mov rsi, [rsi]
		jmp .loop
	.fail:
		xor rax, rax
		ret
	.find:
		mov rax, rsi
		ret