section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс в rdi
exit: 
    mov rdi, rax
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .counter:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .counter
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rsi
    push rdi
    call string_length
    pop rsi
    pop rdi
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rbx, rbx
    mov rcx, 10
  .loop:
    inc rbx

    mov rax, rdi
    xor rdx, rdx
    div rcx
    push rdx
    mov rdi, rax
    test rdi, rdi
    jnz .loop
  .end:
    test rbx, rbx
    jz .r
    pop rdi
    add rdi, 0x30
    dec rbx
    push rbx
    call print_char
    pop rbx
    jmp .end
  .r:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jge print_uint
    neg rdi
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    push rbx
  .loop:
    mov al, byte [rdi]
    test al, al
    je .check_rsi
    mov bl, [rsi]
    cmp al, bl
    jne .false
    inc rdi
    inc rsi
    jmp .loop
  .check_rsi:
    mov al, [rsi]
    test al, al
    je .true
  .false:
    pop rbx
    xor rax, rax
    ret
  .true:
    pop rbx
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .EOF
    mov al, [rsp]
    jmp .END
  .EOF:
    xor rax, rax
  .END:
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx
    dec rsi
    push rdi
    push r12
    push r13
    push r14

  .first_checking:
    mov r12, rdx
    mov r13, rdi
    mov r14, rsi
    call read_char
    mov rdx, r12
    mov rdi, r13
    mov rsi, r14
    cmp al, 0x20
    je .first_checking
    cmp al, 0x9
    je .first_checking
    cmp al, 0xA
    je .first_checking
    test al, al
    je .add_nul

    jmp .write_symbol

  .next_symbol:
    mov r12, rdx
    mov r13, rdi
    mov r14, rsi
    call read_char
    mov rdx, r12
    mov rdi, r13
    mov rsi, r14
    cmp al, 0x9
    je .add_nul
    cmp al, 0xA
    je .add_nul
    test al, al
    je .add_nul

    jmp .write_symbol

  .write_symbol:
    inc rdx
    cmp rdx, rsi
    jg .fail
    mov byte [rdi], al
    inc rdi
    jmp .next_symbol

  .fail:
    pop r12
    pop r13
    pop r14
    pop rax
    xor rax, rax
    ret

  .add_nul:
    pop r12
    pop r13
    pop r14
    mov byte [rdi], 0
    pop rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    push r9
    xor rcx, rcx
    xor r9, r9
  .loop:
    mov al, byte [rdi]
    test al, al
    jz .end
    cmp al, 0x30
    jl .end
    cmp al, 0x39
    jg .end
    sub al, 0x30
    mov bl, al

    mov rax, rcx
    mov rcx, 10
    mul rcx
    mov rcx, rax
    mov rax, rbx
    add rcx, rax

    inc r9
    inc rdi
    jmp .loop

  .end:
    mov rdx, r9
    pop r9
    pop rbx
    mov rax, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov al, byte[rdi]
    cmp al, 0x2D
    je .neg
    call parse_uint
    ret
  .neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push r12
    xor r12, r12
  .loop:
    cmp rax, rdx
    jge .fail
    inc rax
    mov cl, byte [rdi + r12]
    mov byte [rsi + r12], cl
    inc r12
    test cl, cl
    jz .success
    jmp .loop
  .fail:
    xor rax, rax
  .success:
    pop r12
    ret