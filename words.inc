%include "colon.inc"

section .rodata

colon "k", key_with_same_start
db "Same start checking", 0

colon "key", key
db "Some value", 0

colon "key with space", check_space
db "Value with space", 0

colon "key with 123 and +-*/", check_signs
db "Value with numbers (123) and signs (+-*/)", 0

colon "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL", l_one
db "True", 0