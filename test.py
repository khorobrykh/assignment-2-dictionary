import subprocess

stdin = ['k', 'key', 'key with space', 'key with 123 and +-*/', 'key does not exist',
         'VeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKey'
         'VeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKey'
         'VeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKeyVeryLongKey',
         'L' * 255, 'L' * 256]
stdout = ['Same start checking', 'Some value', 'Value with space', 'Value with numbers (123) and signs (+-*/)',
          'Error: The key does not exist', 'Error: The key is too long', 'True', 'Error: The key is too long']

print('Testing...')
results = list()
for index, (inp, out) in enumerate(zip(stdin, stdout), start=1):
    print('------------------------------------\n')
    print(f'\tTest #{index}\n')
    process = subprocess.Popen(['./app'],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)

    output, error = process.communicate(inp)
    if process.returncode == 1:
        result = error.strip() == out.strip()
    else:
        result = output.strip() == out.strip() and process.returncode == 0
    results.append('.' if result else 'F')

    print(f'Input: {inp}\nExpected: {out}\nOutput: {output}\nError: {error}')
    print()
    print('\tTest passed!\n' if result else '\tTest failed!\n')
    process.terminate()

print_res = ''.join(results)
print('------------------------------------\n')
print(f'Results: {print_res}')
print('All tests passed' if len(list(filter(lambda x: x == 'F', results))) == 0 else 'Tests failed')
